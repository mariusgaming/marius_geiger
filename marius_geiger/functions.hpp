class marius
{
	class geiger
	{
		// game will generate functions with name: marius_fnc_<className> ...
		class hideDevice {file = "marius_geiger\fn_deviceHide.sqf";};
		class showDevice {file = "marius_geiger\fn_deviceShow.sqf";};
		class geigerClick {file = "marius_geiger\fn_geigerClick.sqf";};
		class updateDisplay {file = "marius_geiger\fn_geigerDisplay.sqf";};
		class initPlayer {file = "marius_geiger\fn_initPlayer.sqf";};
		class initRadObjects {file = "marius_geiger\fn_initRadObjects.sqf";};
		class radEffects {file = "marius_geiger\fn_radiation.sqf";};
	};
};