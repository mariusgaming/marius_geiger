class geiger {
	idd = 136;
	movingEnable = false;
	duration = 1e+1000;
	name = "geiger";
	onLoad = "with uiNameSpace do {geiger = _this select 0}";

	class controls {
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Marius, v1.063, #Tibasy)
////////////////////////////////////////////////////////

class RscPicture_1200: RscPicture
{
	idc = 1200;

	text = "marius_geiger\device_ca.paa";
	x = 0.257656 * safezoneW + safezoneX;
	y = 0.687 * safezoneH + safezoneY;
	w = 0.308 * safezoneH / 2.8;
	h = 0.308 * safezoneH;
};
class RscStructuredText_1100: RscStructuredText
{
	idc = 1100;
	shadow = 0;

	text = "0.0 mSv/h"; //--- ToDo: Localize;
	x = 0.267011 * safezoneW + safezoneX;
	y = 0.763732 * safezoneH + safezoneY;
	w = 0.0173704 * safezoneH / 0.223534;
	h = 0.0173704 * safezoneH;
	colorText[] = {0,0,0,1};
	colorBackground[] = {-1,-1,-1,0};
	class Attributes {
		color= "#000000";
		align = "right";
		shadow = 0;
	};
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////
	};
};