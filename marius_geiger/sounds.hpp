class click
{
	name = "click";
	sound[] = {"marius_geiger\snd\geigerclick.ogg", 1.25, 1}; // filename, volume, pitch, distance (optional)
	titles[] = {0,""};
};

class warn
{
	name = "warn";
	sound[] = {"marius_geiger\snd\rad_warning.ogg", 0.3, 1};
	titles[] = {0,""};
};