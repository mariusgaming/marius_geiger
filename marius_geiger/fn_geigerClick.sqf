// produces Geiger clicks based on rad strength

while {true} do {

	if (marius_geiger_currentRadiation > 0.01) then {
	
		_clickInterval = 0;

		switch true do {
			case (marius_geiger_currentRadiation > 1800): {_clickInterval = 1};
			case (marius_geiger_currentRadiation > 1200): {_clickInterval = 2};
			case (marius_geiger_currentRadiation > 500): {_clickInterval = 3};
			case (marius_geiger_currentRadiation > 200): {_clickInterval = 4};
			case (marius_geiger_currentRadiation > 100): {_clickInterval = 5};
			case (marius_geiger_currentRadiation > 10): {_clickInterval = 6};
			case (marius_geiger_currentRadiation > 1): {_clickInterval = 7};
			case (marius_geiger_currentRadiation > 0.1): {_clickInterval = 8};
			default {_clickInterval = 9};
		};

		for "_i" from 1 to 60 do { // produce clicking for 0.9 seconds before re-evaluating rad strength
			if (random 10 > _clickInterval) then {playSound "click"};
			sleep 0.015;
		};

	} else {sleep 1}; // wait for rad source if none

};