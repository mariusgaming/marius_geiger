// this script will update the Geiger text, but also give sound warning when reading is critical!
params [["_updateInterval", 1]];

// show device
"l1" cutRsc ["geiger","PLAIN",0.1];

// select control
_display = uiNameSpace getVariable "geiger";
_setText = _display displayCtrl 1100;
_textSize = safeZoneH * 0.44; // 0.8 @1080p

// colors
_meterDisplay =			"<t size='%1' color='#000000'>%2 mSv/h</t>";
_meterDisplayGreen =	"<t size='%1' color='#00ff11'>%2 mSv/h</t>";
_meterDisplayYellow =	"<t size='%1' color='#ffff00'>%2 mSv/h</t>";
_meterDisplayOrange =	"<t size='%1' color='#ff5e00'>%2 mSv/h</t>";
_meterDisplayRed =		"<t size='%1' color='#ad0000'>%2 mSv/h</t>";

_warnGiven = false;

while {true} do {

	// if player died, close device
	if (!alive player) exitWith {call marius_fnc_hideDevice};

	// handle max reading
	_reading = marius_geiger_currentRadiation min 99999.9;

	// cut decimals (converts to String)
	if (_reading >= 1) then {_reading = _reading toFixed 1} else {
		_reading = _reading toFixed 5;
	};

	// give sound warnings and select display color based on rad strength
	_displayText = _meterDisplay;
	_readingNumber = parseNumber _reading;
	switch true do {
		case (_readingNumber >= 2000): {
			_displayText = _meterDisplayRed;
			if (!_warnGiven) then {playSound "warn"; _warnGiven = true}
		};
		case (_readingNumber >= 800): {
			_displayText = _meterDisplayOrange;
			if (!_warnGiven) then {playSound "warn"; _warnGiven = true}
		};
		case (_readingNumber >= 200): {
			_displayText = _meterDisplayYellow;
			if (!_warnGiven) then {playSound "warn"; _warnGiven = true}
		};
		case (_readingNumber < 0.0005): {
			_displayText = _meterDisplayGreen;
			_warnGiven = false;
		};
		default {_warnGiven = false};
	};

	// update display
	_setText ctrlSetStructuredText (parseText format [_displayText, _textSize, _reading]);

	// update delay
	sleep _updateInterval;

};