// primary radiation calculation fnc that should run locally on each player machine (hasInterface == true)

params [
	["_interval",1],
	["_idleInterval",10],
	["_damageType","stab"],
	["_backgroundRadiation", 0.00027], // avg. BR on Earth is 0.00027
	["_protectiveMask", "SP_GasMask_Black"]	// this item will lower rad effects on player
];

// local var for mid calculation
_reading = _backgroundRadiation;

// declare global var so device can access it
marius_geiger_currentRadiation = _backgroundRadiation;

_radiationCheck = {

	params ["_radObjArray","_expa","_expb"];

	{
		_distance = (player distance _x) - ((boundingBoxReal _x select 2) / 2); // half of boundingBox to get object edge
		if (_distance < 120) then { // optimisation
			_reading = _reading + (_expa * (_expb ^ _distance));
			// add some variance
			_reading = _reading + (random (_reading / 2000) - (_reading / 4000));
		};
	} forEach _radObjArray;

};

_radiationAreaCheck = {

	params ["_radObjArray","_expa","_expb"];

	{
		_distance = (player distance _x); // distance to center of area
		if (_distance < 99999) then { // optimization
			_reading = _reading + ((_expa * _distance + _expb) max 0); // no asimptote in this f(x) so use a max 0
			// add some variance
			_reading = _reading + (random (_reading / 2000) - (_reading / 4000));
		};
	} forEach _radObjArray;

};

_radiationDamage = {

	params ["_damage","_damageType"];

	{
		[player, _damage, _x, _damageType] call ace_medical_fnc_addDamageToUnit;
		sleep 0.1; // without sleep 0.1 here the damage will apply twice (ACE bug?)
	} forEach ["Head","Body","Rightarm","Leftarm","Rightleg","Leftleg"];

};

while {true} do {

	// if sources of rad. were deleted but server did not update radSources we must remove Nulls
	radSources = radSources - [objNull];
	radSourcesLow = radSourcesLow - [objNull];
	radAreas = radAreas - [objNull];

	// do we have any sources now?
	if (count (radSources + radSourcesLow + radAreas) > 0) then {

		// if we have rad sources run code on them
		if (count radSources > 0) then {[radSources, 11999.99973, 0.8359587927] call _radiationCheck};
		if (count radSourcesLow > 0) then {[radSourcesLow, 0.79973, 0.833699811] call _radiationCheck};
		if (count radAreas > 0) then {[radAreas, -11.9, 12000] call _radiationAreaCheck};

		// set global combined reading for this player machine
		marius_geiger_currentRadiation = _reading;

		// handle damage of player if radiation reading is higher than:
		switch true do {
			case (_reading > 10000): {[1, _damageType] spawn _radiationDamage};
			case (_reading > 5000): {[0.5, _damageType] spawn _radiationDamage};
			case (_reading > 2000): {
				if !(goggles player isEqualTo _protectiveMask) then {
					if (50 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				} else {
					if (25 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				};
			};
			case (_reading > 800): {
				if !(goggles player isEqualTo _protectiveMask) then {
					if (25 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				} else {
					if (12.5 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				};
			};
			case (_reading > 200): {
				if !(goggles player isEqualTo _protectiveMask) then {
					if (10 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				} else {
					if (5 > random 100) then {[0.2, _damageType] spawn _radiationDamage};
				};
			};
		};

		// reset local calculation and cycle
		_reading = _backgroundRadiation;
		sleep _interval;


	// else wait if no rad sources at all on map
	} else {
		// set global reading
		marius_geiger_currentRadiation = _backgroundRadiation;
		sleep _idleInterval;
	};

};