waitUntil {!isNull player};

// run a calculate radiation script for this player
marius_geiger_radEffects = [] spawn marius_fnc_radEffects;

// if device open var
marius_geiger_geigOpen = false;

// add actions
// hide Geiger
_action = ["Hide Geiger","Hide Geiger Counter","",{call marius_fnc_hideDevice},{marius_geiger_geigOpen}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _action] call ace_interact_menu_fnc_addActionToObject;

// Show Geiger
_action = ["Show Geiger","Show Geiger Counter","",{call marius_fnc_showDevice},{!marius_geiger_geigOpen}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _action] call ace_interact_menu_fnc_addActionToObject;