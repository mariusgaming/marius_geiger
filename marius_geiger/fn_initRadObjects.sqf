// generate array of radiation sources and send to clients
// this should be run on the Server machine only
// you have to manually call this fnc if you want to update the radiation sources mid mission

radSources = [];
radSourcesLow = [];
radAreas = [];

for "_i" from 0 to 9999 do {

	if (!isNil format["radSource_%1",_i]) then {
		if (!isNull (call compile format["radSource_%1",_i])) then {
			radSources pushBack (call compile format["radSource_%1",_i]);
		};
	};

	if (!isNil format["radSourceLow_%1",_i]) then {
		if (!isNull (call compile format["radSourceLow_%1",_i])) then {
			radSourcesLow pushBack (call compile format["radSourceLow_%1",_i]);
		};
	};

};

for "_i" from 0 to 99 do {

	if (!isNil format["radArea_%1",_i]) then {
		if (!isNull (call compile format["radArea_%1",_i])) then {
			radAreas pushBack (call compile format["radArea_%1",_i]);
		};
	};

};

publicVariable "radSources";
publicVariable "radSourcesLow";
publicVariable "radAreas";